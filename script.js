const express = require('express')
const PORT = 3000
let mockDB = [
    {username: "johndoe", password: "johndoe1234"},
    {username: "johnsmith", password: "johnsmith1234"}
]
const APP = express()

APP.use(express.json())
APP.use(express.urlencoded({extended:true}))
// 1 & 2
APP.get("/home", (req,res) => {
    res.status(200).send(`Welcome to the home page`)
})
// 3 & 4
APP.get("/users", (req,res) => {
    res.status(200).send(mockDB)
})

APP.delete("/delete-user", (req, res) => {
    let [user] = mockDB
    res.send(`User ${user.username} has been deleted`)
    
    let index = user.username.indexOf('johndoe')
    mockDB.splice(index,1)
    console.log(mockDB)
})

APP.listen(PORT, ()=>console.log(`Conntected to ${PORT}`))
